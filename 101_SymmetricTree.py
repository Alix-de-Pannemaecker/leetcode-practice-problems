class TreeNode(object):
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution(object):
    def isSymmetric(self, root: TreeNode) -> bool:
        """
        :type root: TreeNode
        :rtype: bool
        """
        def is_mirror(left: TreeNode, right: TreeNode) -> bool:
            if not left and not right: return True
            if not left or not right: return False
            if left.val != right.val: return False
            return is_mirror(left.left, right.right) and is_mirror(left.right, right.left)

        return is_mirror(root, root)
