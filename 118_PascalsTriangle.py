class Solution(object):
    def generate(self, numRows):
        """
        :type numRows: int
        :rtype: List[List[int]]
        """
        res = []
        for i in range(numRows):
            res.append([])
            for j in range(i + 1):
                if j == 0 or j == i:
                    res[i].append(1)
                else:
                    res[i].append(res[i - 1][j - 1] +  res[i - 1][j])
        return res

test = Solution().generate(5)
print(test) #[[1],[1,1],[1,2,1],[1,3,3,1],[1,4,6,4,1]]

test = Solution().generate(1)
print(test) #[[1]]
