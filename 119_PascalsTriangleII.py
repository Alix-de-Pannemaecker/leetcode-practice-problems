class Solution(object):
    def getRow(self, rowIndex):
        """
        :type rowIndex: int
        :rtype: List[int]
        """
        res = []
        for i in range(rowIndex + 1):
            res.append([])
            for j in range(i + 1):
                if j == 0 or j == i:
                    res[i].append(1)
                else:
                    res[i].append(res[i - 1][j - 1] +  res[i - 1][j])
        return res[-1]

test = Solution().getRow(5)
print(test) #[1,4,6,4,1]

test = Solution().getRow(1)
print(test) #[1]
