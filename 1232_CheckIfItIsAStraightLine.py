class Solution(object):
    def checkStraightLine(self, coordinates):
        """
        :type coordinates: List[List[int]]
        :rtype: bool
        """
        if coordinates == []:
            return False
        if len(coordinates) == 2:
            return True

        element = coordinates[0][0]
        count = 0
        for i in range(len(coordinates)):
            if element == coordinates[i][0]:
                count += 1
            else:
                break
        if count == len(coordinates):
            return True

        try:
            m = (coordinates[1][1] - coordinates[0][1]) / (coordinates[1][0] - coordinates[0][0])
        except:
            return False

        for i in range(len(coordinates) - 1):
            try:
                m_check = (coordinates[i + 1][1] - coordinates[i][1]) / (coordinates[i + 1][0] - coordinates[i][0])
            except:
                return False

            if m_check != m:
                return False
        return True


test = Solution().checkStraightLine([[1,2],[2,3],[3,4],[4,5],[5,6],[6,7]])
print(test)

test = Solution().checkStraightLine([[1,1],[2,2],[3,4],[4,5],[5,6],[7,7]])
print(test)

test = Solution().checkStraightLine([[0,0],[0,1],[0,-1]])
print(test)
