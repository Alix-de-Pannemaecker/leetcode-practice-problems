class Solution(object):
    def isPalindrome(self, s):
        """
        :type s: str
        :rtype: bool
        """
        if s == "":
            return True

        s = s.lower()
        res = ""
        for letter in s:
            if letter.isalnum():
                res += letter

        res_rev = list(reversed(res))
        res_rev = "".join(res_rev)

        if res == res_rev:
            return True
        else:
            return False

testing = Solution().isPalindrome("race a car")
print(testing)
