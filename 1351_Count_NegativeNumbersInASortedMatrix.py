class Solution(object):
    def countNegatives(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: int
        """
        neg = 0
        for l in grid:
            length = len(l)
            k = length - 1
            while k >= 0:
                if l[k] < 0:
                    neg += 1
                    k -= 1
                else:
                    break
        return neg

test = Solution().countNegatives([[4,3,2,-1],[3,2,1,-1],[1,1,-1,-2],[-1,-1,-2,-3]])
print(test) #8

test = Solution().countNegatives([[3,2],[1,0]])
print(test) #0
