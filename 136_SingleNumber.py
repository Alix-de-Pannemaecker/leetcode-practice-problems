class Solution(object):
    def singleNumber(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        d = {}

        for n in nums:
            if n in d.keys():
                d[n] += 1
            else:
                d[n] = 1

        for n in nums:
            if d[n] == 1:
                return n

test = Solution().singleNumber([4,1,2,1,2])
print(test) #4

test = Solution().singleNumber([1])
print(test) #1
