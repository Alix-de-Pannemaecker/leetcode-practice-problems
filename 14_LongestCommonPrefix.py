class Solution(object):
    def longestCommonPrefix(self, strs):

        common = ""

        if len(strs) == 1:
            return strs[0]

        if (len(strs) != 1 and "" in strs) or strs == []:
            return common

        common = strs[0]
        min_length = min(len(i) for i in strs)
        common = common[0:min_length]

        for i in range(len(common)):
            for j in range(len(strs)):
                if common[i] not in strs[j][i]:
                    return common[0:i]
        return common

testing = Solution().longestCommonPrefix(["dog","racecar","car"])
print(testing)
