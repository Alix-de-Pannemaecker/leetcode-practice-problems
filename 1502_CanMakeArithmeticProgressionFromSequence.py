class Solution(object):
    def canMakeArithmeticProgression(self, arr):
        """
        :type arr: List[int]
        :rtype: bool
        """
        if len(arr) == 0 or len(arr) == 1:
            return False
        if len(arr) == 2:
            return True

        arr.sort()
        r = arr[1] - arr[0]

        k = len(arr) - 1
        while k > 0:
            if arr[k] - arr[k - 1] != r:
                return False
            k -= 1
        return True


test = Solution().canMakeArithmeticProgression([3,5,1])
print(test)
test = Solution().canMakeArithmeticProgression([1,2,4])
print(test)
