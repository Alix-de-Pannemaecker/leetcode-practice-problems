class Solution(object):
    def threeSum(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """
        res = []
        if len(nums) == 3 and sum(nums) == 0:
            res.append(nums)
            return res

        count_0 = 0
        for i in range(len(nums)):
            if nums[i] == 0:
                count_0 += 1

        for i in range(len(nums)):
            for j in range(len(nums)):
                for k in range (len(nums)):
                    if nums[i] + nums[j] + nums[k] == 0 and i!=j and j!=k and i!=k:
                        res.append([nums[i],nums[j],nums[k]])

        final = []
        for i in range(len(res)):
            if [res[i][0],res[i][1],res[i][2]] not in final \
                and [res[i][1],res[i][2],res[i][0]] not in final \
                    and [res[i][2],res[i][0],res[i][1]] not in final \
                     and [res[i][0],res[i][2],res[i][1]] not in final \
                      and [res[i][1],res[i][0],res[i][2]] not in final \
                       and [res[i][2],res[i][1],res[i][0]] not in final \
                        and [res[i][1],res[i][0],res[i][2]] not in final \
                         and [res[i][2],res[i][1],res[i][0]] not in final \
                          and [res[i][0],res[i][2],res[i][1]] not in final:
                final.append(res[i])
        return final


test = Solution().threeSum([-1,0,1,2,-1,-4])
print(test) #[[-1,-1,2],[-1,0,1]]

test = Solution().threeSum([0,1,1])
print(test) #[]

test = Solution().threeSum([0,0,0])
print(test) #[[0,0,0]]

test = Solution().threeSum([-1,0,1])
print(test) #[[-1,0,1]]

test = Solution().threeSum([1,2,-2,-1])
print(test) #[]
