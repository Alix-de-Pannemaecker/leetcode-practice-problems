class Solution(object):
    def majorityElement(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if len(nums) == 0:
            return None
        if len(nums) == 1:
            return nums[0]

        d = {}
        for n in nums:
            if n not in d.keys():
                d[n] = 1
            else:
                d[n] += 1
                if d[n] >= len(nums) / 2:
                    return n

test = Solution().majorityElement([3,2,3])
print(test) #3

test = Solution().majorityElement([2,2,1,1,1,2,2])
print(test) #2
