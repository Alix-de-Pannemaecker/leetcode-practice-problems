class Solution(object):
    def titleToNumber(self, columnTitle):
        """
        :type columnTitle: str
        :rtype: int
        """
        alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

        d = {}
        for i in range(len(alphabet)):
            d[alphabet[i]] = i + 1

        if len(columnTitle) == 1:
            return d[columnTitle]

        res = 0
        for i in range(len(columnTitle)):
            res = res * 26
            res += d[columnTitle[i]]
        return res

test = Solution().titleToNumber("A")
print(test) #26

test = Solution().titleToNumber("AB")
print(test) #28 = 26 + 2

test = Solution().titleToNumber("ZY")
print(test) #701 = 26 * 26 + 25
