class Solution:
    # @param n, an integer
    # @return an integer
    def reverseBits(self, n):
        reversed_n = ""
        for i in range(len(n) - 1, -1, -1):
            reversed_n += n[i]

        res = int("".join(str(x) for x in reversed_n), 2)
        return res

test = Solution().reverseBits("00000010100101000001111010011100")
print(test) #964176192

test = Solution().reverseBits("11111111111111111111111111111101")
print(test) #3221225471
