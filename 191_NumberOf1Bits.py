class Solution(object):
    def hammingWeight(self, n):
        """
        :type n: int
        :rtype: int
        """
        count = 0
        n = bin(n)

        for num in n:
            if num == "1":
                count += 1
        return count
