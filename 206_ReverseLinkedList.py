
#SOLUTION WITHOUT NODES-----------------------------------------------
# class Solution(object):
#     def reverseList(self, head):
#         """
#         :type head: ListNode
#         :rtype: ListNode
#         """
#         if len(head) <= 1:
#             return head
#         l = []
#         for i in range(len(head) - 1,-1,-1):
#             l.append(head[i])
#         return l

#SOLUTION WITH NODES----------------------------------------------------
# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    def reverseList(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """

        # Iteration approach
        # Run one pointer p in list. At each iteration:
        #     1. n = p.next
        #     2. p.next = n.next, jump cross n
        #     3. n.next = head, n prepend to the front
        #     4. head = n, reassign head

        if head == None:
            return None

        p = head
        while p.next:
            n = p.next
            p.next = n.next
            n.next = head
            head = n

        return head

    def printList(self, head):
        arr= []
        p=head
        while p:
            arr.append(p.val)
            p = p.next

        print(arr)


NodeList = ListNode(1)
NodeList.next = ListNode(2)
NodeList.next.next = ListNode(3)
NodeList.next.next.next = ListNode(4)
NodeList.next.next.next.next = ListNode(5)

NodeList = Solution().reverseList(NodeList)
Solution().printList(NodeList)

NodeList = Solution().reverseList(NodeList)
Solution().printList(NodeList)

NodeList = Solution().reverseList(NodeList)
Solution().printList(NodeList)

NodeList = Solution().reverseList(NodeList)
Solution().printList(NodeList)

NodeList = Solution().reverseList(NodeList)
Solution().printList(NodeList)
