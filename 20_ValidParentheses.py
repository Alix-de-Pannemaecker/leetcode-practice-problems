class Solution(object):
    def isValid(self, s):
        """
        :type s: str
        :rtype: bool
        """
        if s == "":
            return False

        while "()" in s or "[]" in s or "{}" in s:
            s = s.replace("()","")
            s = s.replace("[]","")
            s = s.replace("{}","")

        if s == "":
            return True
        return False


testing = Solution().isValid("(({[]})")
print(testing)
