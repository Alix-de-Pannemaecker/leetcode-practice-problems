class Solution(object):
    def containsDuplicate(self, nums):
        d = {}
        for n in nums:
            if d.get(n, 0) == 1:
                return True
            else:
                d[n] = 1
        return False

testing = Solution().containsDuplicate([1,1,1,3,3,4,3,2,4,2])
print(testing)
