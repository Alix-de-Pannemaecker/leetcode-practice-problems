class Solution(object):
    def summaryRanges(self, nums):
        """
        :type nums: List[int]
        :rtype: List[str]
        """
        if len(nums) == 0 or len(nums) == 1:
            return [str(n) for n in nums]

        nums.append(nums[-1])
        l = []
        sub = [nums[0]]
        for i in range(len(nums) - 1):
            if nums[i + 1] - nums[i] != 1:
                sub.append(nums[i])
                l.append(sub)
                sub = [nums[i + 1]]

        res = []
        for n in l:
            if n[0] == n[1]:
                res.append(str(n[0]))
            else:
                res.append(f"{n[0]}->{n[1]}")

        return res

test = Solution().summaryRanges([0,1,2,4,5,7])
print(test)

test = Solution().summaryRanges([0,2,3,4,6,8,9])
print(test)
