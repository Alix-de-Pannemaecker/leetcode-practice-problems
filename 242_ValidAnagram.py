class Solution(object):
    def isAnagram(self, s, t):
        """
        :type s: str
        :type t: str
        :rtype: bool
        """
        if s == "" and t == "":
            return True
        elif (s == "" and t != "") or (s != "" and t == ""):
            return False
        elif len(s) != len(t):
            return False

        ds = {}
        for letter in s:
            if letter not in ds.keys():
                ds[letter] = 1
            else:
                ds[letter] += 1

        dt = {}
        for letter in t:
            if letter not in dt.keys():
                dt[letter] = 1
            else:
                dt[letter] += 1

        for ks in ds.keys():
            if ks not in dt.keys():
                return False
            elif  ds[ks] != dt[ks]:
                return False
        return True

test = Solution().isAnagram("anagram", "nagaram")
print(test)

test = Solution().isAnagram("rat", "car")
print(test)
