class Solution(object):
    def removeDuplicates(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if nums == []:
            return 0

        uniqueArr = []
        d = {}
        k = 0
        end = len(nums)
        for i in range(len(nums)):
            if d.get(nums[i], 0) == 0:
                d[nums[i]] = 1
                uniqueArr.append(nums[i])
                k += 1

        j=0
        for i in range(len(nums)):
            if i < len(uniqueArr):
                nums[i] = uniqueArr[j]
                j += 1
            else:
                nums[i] = "_"

        return k

testing = Solution().removeDuplicates([0,0,1,1,1,2,2,3,3,4])
print(testing)
