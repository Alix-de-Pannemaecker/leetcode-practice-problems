class Solution(object):
    def findNonMinOrMax(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if len(nums) <= 2:
            return -1

        for n in nums:
            if n != min(nums) and n != max(nums):
                return n

test = Solution().findNonMinOrMax([3,2,1,4])
print(test) #3

test = Solution().findNonMinOrMax([1,2])
print(test) #-1
