class Solution(object):
    def moveZeroes(self, nums):
        """
        :type nums: List[int]
        :rtype: None Do not return anything, modify nums in-place instead.
        """
        j = 0
        for num in nums:
            if num != 0:
                nums[j] = num
                j += 1

        for i in range(j, len(nums)):
            nums[i] = 0

        return nums

test = Solution().moveZeroes([0,1,0,3,12])
print(test) #[1, 3, 12, 0, 0]

test = Solution().moveZeroes([0])
print(test) #[0]
