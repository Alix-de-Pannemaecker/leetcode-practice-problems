class Solution(object):
    def reverseString(self, s):
        """
        :type s: List[str]
        :rtype: None Do not return anything, modify s in-place instead.
        """
        if s == [] or len(s) == 1:
            return s

        l = len(s)
        for i in range(l // 2):
            s[i], s[l - 1 - i] = s[l - 1 - i], s[i]
        return(s)


test = Solution().reverseString(["h","e","l","l","o"])
print(test)
