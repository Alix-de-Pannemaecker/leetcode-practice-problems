def searchInsert(nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: int
        """

        if len(nums)==1:
            if target > nums[0]:
                return 1
            else:
                return 0

        for i in range(len(nums) - 1):
            if nums[i] == target:
                return i
            elif target > nums[i] and target < nums[i + 1]:
                return i + 1
            elif target > nums[-1]:
                return len(nums)
            elif target < nums[0]:
                return 0

        if target == nums[len(nums) - 1]:
            return len(nums) - 1

print(searchInsert([1, 3], 3))
