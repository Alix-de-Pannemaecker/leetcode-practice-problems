class Solution(object):
    def lengthOfLastWord(self, s):
        """
        :type s: str
        :rtype: int
        """
        l = s.split(" ")
        i = len(l) - 1
        while i >=0 :
            if l[i] != "":
                return len(l[i])
            i -= 1


testing = Solution().lengthOfLastWord("Hello World")
print(testing)

testing = Solution().lengthOfLastWord("   fly me   to   the moon  ")
print(testing)

testing = Solution().lengthOfLastWord("luffy is still joyboy")
print(testing)
