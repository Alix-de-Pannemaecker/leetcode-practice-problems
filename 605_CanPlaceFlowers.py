class Solution(object):
    def canPlaceFlowers(self, flowerbed, n):
        """
        :type flowerbed: List[int]
        :type n: int
        :rtype: bool
        """
        if n == 0 or (flowerbed == [0] and n == 1):
            return True

        seq = [0,0,0]
        count = 0
        flowerbed = [0] + flowerbed + [0]
        for i in range(1,len(flowerbed) - 1):
            if flowerbed[i-1:i+2] == seq:
                flowerbed[i] = 1
                count += 1
            if count >= n:
                return True
        return False

test = Solution().canPlaceFlowers([1,0,0,0,0,1],1)
print(test) #True
test = Solution().canPlaceFlowers([1,0,0,0,1],2)
print(test) #False
