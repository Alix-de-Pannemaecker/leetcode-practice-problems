class Solution(object):
    def search(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: int
        """
        if target > nums[-1]:
            return -1

        left_index = 0
        right_index = len(nums)
        while left_index <= right_index:
            mid_index = left_index + (right_index - left_index) //2
            if nums[mid_index] > target:
                right_index = mid_index - 1
            elif nums[mid_index] < target:
                left_index = mid_index + 1
            else:
                return mid_index
        return -1


testing = Solution().search([-1,0,3,5,9,12],9)
print(testing) #4

testing = Solution().search([-1,0,3,5,9,12],2)
print(testing) #-1
