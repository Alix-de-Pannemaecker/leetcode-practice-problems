class Solution(object):
    def climbStairs(self, n):
        """
        :type n: int
        :rtype: int
        """
        if n == 0 or n == 1 or n == 2:
            return n

        prevprev = 1
        prev = 2
        current = 0

        for step in range(3, n +1):
            current = prevprev + prev

            prevprev = prev
            prev = current

        return current


test = Solution().climbStairs(5)
print(test)
