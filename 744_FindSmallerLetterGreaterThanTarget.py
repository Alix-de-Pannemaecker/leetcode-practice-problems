class Solution(object):
    def nextGreatestLetter(self, letters, target):
        """
        :type letters: List[str]
        :type target: str
        :rtype: str
        """
        k = len(letters) - 1
        if target >= letters[k]:
            return letters[0]

        while k >= 0:
            if letters[k] > target:
                min = letters[k]

            else:
                return min
            k -= 1
        return min

test = Solution().nextGreatestLetter(["c","f","j"],"a") #c
print(test)

test = Solution().nextGreatestLetter(["c","f","j"],"c") #f
print(test)

test = Solution().nextGreatestLetter(["x","x","y","y"],"z") #x
print(test)

test = Solution().nextGreatestLetter(["c","f","j"],"d") #f
print(test)
