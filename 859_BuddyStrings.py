class Solution(object):
    def buddyStrings(self, s, goal):
        """
        :type s: str
        :type goal: str
        :rtype: bool
        """
        s_list = []

        if len(s) != len(goal):
            return False
        if s == goal and s[0] == s[1] and len(s) == 2:
            return True
        # if s == goal and len(s) >= 3:
        #     return True

        for i in range(len(s)):
            if s[i] != goal[i]:
                s_list.append(i)

        new_s = []
        for i in range(len(s_list)):
            for j in range(len(s_list)):
                if i!=j:
                    s_swap = list(s)
                    s_swap[s_list[i]], s_swap[s_list[j]] = s_swap[s_list[j]], s_swap[s_list[i]]
                    s_swap = "".join(s_swap)
                    if s_swap not in new_s:
                        new_s.append(s_swap)
        for swap in new_s:
            if swap == goal:
                return True
        return False


test = Solution().buddyStrings("ab","ba")
print(test) #True

test = Solution().buddyStrings("ab","ab")
print(test) #False

test = Solution().buddyStrings("aa","aa")
print(test) #True

test = Solution().buddyStrings("aaaaaaabc","aaaaaaacb")
print(test) #True
