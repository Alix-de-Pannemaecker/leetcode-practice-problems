# You are given a dictionary with two keys a and b that hold integers as their values.
# Without declaring any other variable, swap the value of a with the value of b and vice versa.
# Note: Return the dictionary after editing it.

def swap_values(numbers):
  numbers['a'], numbers['b'] = numbers['b'], numbers['a']
  return numbers

numbers = {
  'a':3,
  'b':4
}

print(swap_values(numbers))


# Write a Python function called max_profit that takes a list of integers, where the i-th integer represents the price of a given stock on day i, and returns the maximum profit you can achieve by buying and selling the stock.
# You may complete, at most, two complete buy/sell transactions to maximize profits on a stock.
# Input:
# prices = [3, 3, 5, 0, 0, 3, 1, 4]
# Output: 6
# Explanation:
# Buy on day 4 (price = 0) and sell on day 6 (price = 3): profit = 3 - 0 = 3. Then buy again on day 7 (price = 1) and sell on day 8 (price = 4): profit = 4 - 1 = 3. Total profit: 3 + 3 = 6.

# def max_profit(prices):
#     pass
